#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
ASIMPLS algorithm 
Training Part

@authors: Pierre Thivel & Vincent P. Martin 
(Univ. Bordeaux, CNRS, Bordeaux INP, LaBRI, UMR 5800, F-33400, Talence, France)
@date: May 2019

@param X the dataset used to train the algorithm
@param y the associated label of the dataset
@param k the number of components used
@return w, T, U, V, Q matrix used for the next functions
"""

import numpy as np

def Train(X, y, k):
    [n,p] = np.shape(X)
    E_i = np.array(X)
    F_i = np.array(y)[:, np.newaxis]
    u_i = np.array(y)[:, np.newaxis]
    w, T, U, V, Q = np.zeros((p,k)), np.zeros((n,k)), np.zeros((n,k)), [], []

    for i in range(k):
        w_i = np.dot(E_i.T, u_i)/np.dot(u_i.T, u_i) 
        w_i = w_i /np.linalg.norm(w_i)
        t_i = np.dot(E_i, w_i)
        c_i = np.dot(F_i.T, t_i)/np.dot(t_i.T, t_i)

        u_i = np.dot(F_i, c_i)
        v_i = np.dot(u_i.T, t_i)/np.dot(t_i.T, t_i) 
        q_i = np.dot(F_i.T, u_i)/np.dot(u_i.T, u_i) 
        E_i = E_i - (t_i * np.dot(t_i.T, E_i))/np.dot(t_i.T, t_i)
        F_i = F_i - v_i*t_i*q_i

        w[:,i] = w_i.flatten()
        T[:,i]=t_i.flatten()
        U[:,i] = u_i.flatten()
        V.append(v_i)
        Q.append(q_i)

    V= np.array(V).flatten()
    Q = np.array(Q).flatten()
    return (w, T, U, V, Q)

