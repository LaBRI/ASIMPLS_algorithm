#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
ASIMPLS algorithm
Test part

@authors: Pierre Thivel & Vincent P. Martin 
(Univ. Bordeaux, CNRS, Bordeaux INP, LaBRI, UMR 5800, F-33400, Talence, France)
@date: May 2019

@param X_test the test dataset
@param w, m, k and b' matrix and coefficient pre calculated
@return predicted labels for the X_test dataset
"""

import numpy as np


def Test(X_test, w, m, k, b_prime):
    S_hat = np.array(X_test)
    N = S_hat.shape[0]

    T = np.zeros((N, k))
    for i in range(k):
        t_hat_i = np.dot(S_hat, w[:,i])
        T[:, i] = t_hat_i.flatten()
        t_hat_i = np.reshape(t_hat_i,(-1,1))
        S_hat = S_hat - t_hat_i * np.dot(t_hat_i.T, S_hat) / np.dot(t_hat_i.T, t_hat_i)

    Y_temp = np.zeros(N)
    for i in range(k):
        Y_temp[:] = Y_temp[:]+ m[i]* T[:, i].flatten()
        
    Y_tilde = Y_temp - np.ones((N,1))*b_prime
    y_hat = np.sign(Y_tilde)

    return np.array(y_hat[0])
