#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
ASIMPLS algorithm
Tunning part

@authors: Pierre Thivel & Vincent Martin 
(Univ. Bordeaux, CNRS, Bordeaux INP, LaBRI, UMR 5800, F-33400, Talence, France)
@date: May 2019

@param X the dataset used to tune parameters
@param y the associated labels
@param k, T, V, Q the components and matrix previously computed
@return m computed matrix
@return b_prime tuned coefficient
"""

import numpy as np
from sklearn.metrics import balanced_accuracy_score

def Tune(X, y, k, T, V, Q):
    E_i = np.array(X_t)
    N = E_i.shape[0]

    # == M_0, r_0, c ==
    M_p = []
    for j in range(0, N):
        M_p.append(E_i[j] * np.where(np.array(y)==1, 1, 0)[j])

    M_p = np.array(M_p)
    M_n = E_i - M_p
    y = list(y)
    M_0 = []
    M_c = []
    for i in range(E_i.shape[1]):
        M_0.append(sum(M_p[:, i]))

    M_0 = np.array(M_0)/y.count(1)
    for i in range(E_i.shape[1]):
        M_c.append(sum(M_n[:, i]))

    M_c = np.array(M_c)/y.count(0)

    r_0 =  np.linalg.norm(np.std(M_p))
    r_c =  np.linalg.norm(np.std(M_n))

    c = sum(np.sqrt(np.abs(M_0 - M_c))) / (r_0 + r_c)

    # == delta optimal ==
    
    y_cible = [1 if y[i]==1 else -1 for i in range(len(y))]
    m = V * Q
    m = m.flatten()
    realisations = 1000
    delta = np.linspace(-1, 1, num =realisations)
    results = []

    for i in range(realisations):
        b_prime = delta[i] * m[0] * (sum(np.sqrt(np.abs(M_0))) - r_0*c)

        Y_temp = np.zeros(N)
        for j in range(k):
            Y_temp[:] = Y_temp[:]+ m[j]* T[:, j].flatten()

        Y_tilde = Y_temp - b_prime
        y_hat = np.sign(Y_tilde).flatten()
        results.append(balanced_accuracy_score(y_cible, y_hat))

    index_max = results.index(max(results))
    Delta = delta[index_max]
    b_prime = Delta*m[0]*(sum(np.sqrt(np.abs(M_0))) - r_0*c)


    return m, b_prime
