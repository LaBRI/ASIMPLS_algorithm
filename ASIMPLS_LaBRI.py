#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
ASIMPLS algorithm

@authors: Pierre Thivel & Vincent Martin 
(Univ. Bordeaux, CNRS, Bordeaux INP, LaBRI, UMR 5800, F-33400, Talence, France)
@date: May 2019

@param X_train, X_dev, X_test the datasets
@param y_train, y_dev, y_test the associated labels
@print results of the scores in train vs dev and train+dev vs test
"""

import numpy as np
from sklearn.metrics import balanced_accuracy_score
from sklearn.preprocessing import StandardScaler, normalize

from ASIMPLS_train import Train
from ASIMPLS_tunning import Tune
from ASIMPLS_test import Test

X_train, x_dev, X_test
y_train, y_dev, y_test
k = 5

# == Scale ==
scaler = StandardScaler()
scaler.fit(X_train, y_train)
X_train_scaled = scaler.transform(X_train_scaled)
X_dev_scaled = scaler.transform(X_dev_scaled)

# == Normalisation ==
X_train_scaled = normalize(X_train_scaled)
X_dev_scaled = normalize(X_dev_scaled)


#%% Train vs Dev
# == TRAIN ==
w, T, U, V, Q = Train(X_train_scaled, y_train, k)


# == TUNNING ==
m, b_prime = Tune(X_train_scaled, y_train, k, T, V, Q)

y_predict = Test(X_dev_scaled, w, m, k, b_prime)

y_dev_sign = [1 if y_dev[i]==1 else -1 for i in range(len(y_dev))]

score_trainvsdev = balanced_accuracy_score(y_dev_sign, list(y_predict))
print("score Train vs Dev :", score_trainvsdev)


#%% Train+Dev vs Test

X_trainDev = np.vstack((X_train, X_dev))
y_trainDev = list(y_train) + list(y_dev)

# == Scale ==
scaler = StandardScaler()
scaler.fit(X_trainDev, y_trainDev)
X_train = scaler.transform(X_trainDev)
X_test = scaler.transform(X_test)

# == Normalisation ==
X_trainDev = normalize(X_train)
X_test = normalize(X_test)

# == Train ==
w, T, U, V, Q = Train(X_trainDev, y_trainDev, k)

# == Tunning ==
# We keap the previous b', already tuned
m, _ = Tune(X_trainDev, y_trainDev, k, T, V, Q)

y_predict = Test(X_test, w, m, k, b_prime)

y_test_sign = [1 if y_test[i]==1 else -1 for i in range(len(y_test))]

score_traindevvstest =  balanced_accuracy_score(y_test_sign, list(y_predict))
print("score Train+Dev vs Test :", score_traindevvstest)













