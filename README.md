  GNU nano 4.2                                                                                            README.md
# ASIMPLS algorithm

## Environement

Made with Python 3.7.3 and libraries :
  - numpy 1.16.3
  - sklearn 0.21.0

## Purpose

The ASIMPLS algorithm is a supervised learning model adapted from the partial least squares method.

It can efficiently perform a linear classification, and be trained to futur classification.

The algorithm is subdivided in three parts : Train, Tune and Test.


## Algorithm description

ASIMPLS is fully described in our pdf documentation : ASIMPLS algorithm.

## Installation

```sh
$ pip install numpy==1.16.3
$ pip install sklearn==0.21.0
```

## References

  - D.Jong, "SIMPLS: An alternative approach to partial least squares regression", Chemometrics andIntelligent Laboratory Systems, 1993.
  - H.Qu et al., "An asymmetric classifier based on partial least squares", Pattern Recognition, 2010.
  - D.Huang et al., "Speaker State Classification Based on Fusion of Asymmetric Simple Partial Least Squares (SIMPLS) and Support Vector Machines", Comput. Speech Lang, 2014.
  - V.P.Martin et al., "Towards at-home automatic sleepinessmeasurement through speech forfollowing-up patients with chronic sleepdisorders", Information Technology, 2019

