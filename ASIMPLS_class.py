#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
ASIMPLS algorithm, by LaBRI (Bordeaux, FRANCE)

@authors: Pierre Thivel & Vincent Martin
@date: May 2019
"""

import numpy as np
from sklearn.metrics import balanced_accuracy_score
from sklearn.preprocessing import StandardScaler, normalize



#%% ASIMPLS algorithm, by LaBRI
def Train(X, y, k):
    [n,p] = np.shape(X)
    E_i = np.array(X)
    F_i = np.array(y)[:, np.newaxis]
    u_i = np.array(y)[:, np.newaxis]
    w, T, U, V, Q = np.zeros((p,k)), np.zeros((n,k)), np.zeros((n,k)), [], []

    for i in range(k):
        w_i = np.dot(E_i.T, u_i)/np.dot(u_i.T, u_i) #/ np.dot(u_i.T, u_i)[0][0]
        w_i = w_i /np.linalg.norm(w_i)# np.dot(w_i.T, w_i)
        t_i = np.dot(E_i, w_i)
        c_i = np.dot(F_i.T, t_i)/np.dot(t_i.T, t_i) #np.dot(t_i.T, t_i)[0][0]

        u_i = np.dot(F_i, c_i)
        v_i = np.dot(u_i.T, t_i)/np.dot(t_i.T, t_i) #np.dot(u_i.T, u_i)[0][0]
        q_i = np.dot(F_i.T, u_i)/np.dot(u_i.T, u_i) #np.dot(u_i.T, u_i)[0][0]
        E_i = E_i - (t_i * np.dot(t_i.T, E_i))/np.dot(t_i.T, t_i)#np.dot(t_i.T, t_i)[0][0]
        F_i = F_i - v_i*t_i*q_i

        w[:,i] = w_i.flatten()
        T[:,i]=t_i.flatten()
        U[:,i] = u_i.flatten()
        V.append(v_i)
        Q.append(q_i)

    V= np.array(V).flatten()
    Q = np.array(Q).flatten()
    return (w, T, U, V, Q)


def Tune(X_t, y_t, k, T, V, Q):
    E_i = np.array(X_t)
    N = E_i.shape[0]

    M_p = []
    for j in range(0, N):
        M_p.append(E_i[j] * np.where(np.array(y_t)==1, 1, 0)[j])
    M_p = np.array(M_p)
    M_n = E_i - M_p
    y_t = list(y_t)
    M_0 = []
    M_c = []
    for i in range(E_i.shape[1]):
        M_0.append(sum(M_p[:, i]))
    M_0 = np.array(M_0)/y_t.count(1)
    for i in range(E_i.shape[1]):
        M_c.append(sum(M_n[:, i]))
    M_c = np.array(M_c)/y_t.count(0)
    r_0 =  np.linalg.norm(np.std(M_p))
    r_c =  np.linalg.norm(np.std(M_n))

    c = sum(np.sqrt(np.abs(M_0 - M_c))) / (r_0 + r_c)

    y_cible = [1 if y_t[i]==1 else -1 for i in range(len(y_t))]
    m = V * Q
    m = m.flatten()
    realisations = 1000
    delta = np.linspace(-1, 1, num =realisations)
    results = []
    for i in range(realisations):
        b_prime = delta[i] * m[0] * (sum(np.sqrt(np.abs(M_0))) - r_0*c)
        Y_temp = np.zeros(N)
        for j in range(k):
            Y_temp[:] = Y_temp[:]+ m[j]* T[:, j].flatten()
        Y_tilde = Y_temp - b_prime
        y_hat = np.sign(Y_tilde).flatten()
        results.append(balanced_accuracy_score(y_cible, y_hat))

    index_max = results.index(max(results))
    Delta = delta[index_max]
    b_prime = Delta*m[0]*(sum(np.sqrt(np.abs(M_0))) - r_0*c)

    return m, b_prime


def Test(X_test, w, m, k, b_prime):
    S_hat = np.array(X_test)
    N = S_hat.shape[0]
    T = np.zeros((N, k))
    for i in range(k):
        t_hat_i = np.dot(S_hat, w[:,i])
        T[:, i] = t_hat_i.flatten()
        t_hat_i = np.reshape(t_hat_i,(-1,1))
        S_hat = S_hat - t_hat_i * np.dot(t_hat_i.T, S_hat) / np.dot(t_hat_i.T, t_hat_i)

    Y_temp = np.zeros(N)
    for i in range(k):
        Y_temp[:] = Y_temp[:]+ m[i]* T[:, i].flatten()
        
    Y_tilde = Y_temp - np.ones((N,1))*b_prime
    y_hat = np.sign(Y_tilde)

    return np.array(y_hat[0])


class ASIMPLS:
    """
    The ASIMPLS algorithm is a supervised learning model adapted from the partial least squares method.
    It can efficiently perform a linear classification, and be trained to futur classification.
    
    Attributes :
        -- datasets train, develop and test
        -- associated labels
    Return :
        -- predicted label for a test dataset, with Predict() function
    """
    
    def __init__(self, X_train, X_dev, X_test, y_train, y_dev):
        self.X_train = X_train
        self.X_dev = X_dev
        self.X_trainDev = np.vstack((self.X_train, self.X_dev))
        self.X_test = X_test
        
        self.y_train = y_train
        self.y_dev = y_dev
        self.y_trainDev = list(self.y_train) + list(self.y_dev)
        
        self.k = 1
        self.b_prime = 0

        
    def Scale(self):
        """
        This function is optional, and used for scaling datas.
        (recommanded).
        """
        # == scale Train vs Dev  ==
        scaler = StandardScaler()
        scaler.fit(self.X_train, self.y_train)
        self.X_train = scaler.transform(self.X_train)
        self.X_dev = scaler.transform(self.X_dev)
        
        # == scale Train+Dev vs Test
        scaler = StandardScaler()
        scaler.fit(self.X_trainDev, self.y_trainDev)
        self.X_trainDev = scaler.transform(self.X_trainDev)
        self.X_test = scaler.transform(self.X_test)
        
        
    def Norm(self):
        """
        This function is also optional, and used to norm datas.
        """
        self.X_train = normalize(self.X_train)
        self.X_dev= normalize(self.X_dev)
        self.X_trainDev = normalize(self.X_trainDev)
        self.X_test = normalize(self.X_test)
            
        
        
    def Trainning(self, k=1):
        """
        This function train the algorithm in order to tune parametres.
        Prints the balanced accuracy score.
        
        @return: predicted labels for the develop dataset
        """
        self.k = k
        # == TRAIN ==
        w, T, U, V, Q = Train(self.X_train, self.y_train, self.k)
        
        # == TUNNING ==
        m, self.b_prime = Tune(self.X_train, self.y_train, self.k, T, V, Q)
        
        y_predict = Test(self.X_dev, w, m, self.k, self.b_prime)
        
        y_dev_sign = [1 if self.y_dev[i]==1 else -1 for i in range(len(self.y_dev))]
        score_trainvsdev = balanced_accuracy_score(y_dev_sign, list(y_predict))
        print("score Train vs Dev :", score_trainvsdev)
            
        return y_predict, self.b_prime
                    
    
    def Predict(self, y_test=[]):
        """
        Need a previous call of "Trainning" before use.
        Used to predict labels of a test dataset, and eventually scoring the 
        predict if labels are given.
        
        @return: predicted labels for the test dataset
        """
        # == Train ==
        w, T, U, V, Q = Train(self.X_trainDev, self.y_trainDev, self.k)
        
        # == Tunning ==
        m, _ = Tune(self.X_trainDev, self.y_trainDev, self.k, T, V, Q)
        
        y_predict = Test(self.X_test, w, m, self.k, self.b_prime)
        
        if len(y_test)>0:
            y_test_sign = [1 if y_test[i]==1 else -1 for i in range(len(y_test))]        
            score_traindevvstest =  balanced_accuracy_score(y_test_sign, list(y_predict))
            print("score Train+Dev vs Test :", score_traindevvstest)

        return y_predict


#%% TESTING the ASIMPLS algorithm

path = '/'
X_train = np.load(path+'X_train.npy')    
X_dev = np.load(path+'X_dev.npy')
X_test = np.load(path+'X_test.npy')

y_train = np.load(path+'y_train.npy')
y_dev = np.load(path+'y_dev.npy')
y_test = np.load(path+'y_test.npy')


# Train+Dev vs Test
asimpls = ASIMPLS(X_train, X_dev, X_test, y_train, y_dev)

asimpls.Scale()

_, b_prime = asimpls.Trainning(5)
print("Parametre obtimal b' retenu :", b_prime)

_ = asimpls.Predict(y_test)

















